### INSTALL
```bash
make build
make init-dev
make up
```

### APP CONSOLE
```bash
make console
```

### GRAYLOG CONFIG
1. login to grayloga ui localhost:9005 `user: admin | pass: admin`
2. open `settings | inputs | select input` - add `gelf UDP` global settings
