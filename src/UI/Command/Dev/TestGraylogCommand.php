<?php

namespace App\UI\Command\Dev;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class TestGraylogCommand extends Command
{
    protected static $defaultName = 'test:graylog';

    private $messageBus;
    private $logger;

    public function __construct(MessageBusInterface $messageBus, LoggerInterface $logger)
    {
        $this->messageBus = $messageBus;
        $this->logger = $logger;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->notice("notice", ['data'=>'example']);
        $this->logger->info("info", ['data'=>'example']);
        $this->logger->warning("warning", ['data'=>'example']);
        $this->logger->error("error", ['data'=>'example']);
        $this->logger->critical("critical", ['data'=>'example']);

        return 0;
    }
}