<?php

namespace App\UI\Controller;

use App\Application\Message\Command\Item\CreateItem;
use App\Application\Message\Command\Item\UpdateItemsOrderNo;
use App\Domain\Item\ItemRepositoryInterface;
use App\Domain\Item\Query\GetItemsFromFolderQueryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ItemController extends AbstractController
{
    private $loggerItem;

    public function __construct(LoggerInterface $loggerItem)
    {
        $this->loggerItem = $loggerItem;
    }

    /**
     * @Route("/item", methods={"GET"})
     * @Route("/item/{parentId}", methods={"GET"})
     */
    public function getItemByParentId(GetItemsFromFolderQueryInterface $getItemsFromFolderQuery, int $parentId = 0): JsonResponse
    {
        try {
            $itemsList = $getItemsFromFolderQuery->getResult($parentId);

            return new JsonResponse($itemsList, Response::HTTP_OK);
        } catch (\Exception $e) {
            $this->log($e);

            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/item", methods={"POST"})
     */
    public function createItem(Request $request, MessageBusInterface $commandBus): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        try {
            $message = new CreateItem($data['parentId'], $data['name'], $data['type']);

            $commandBus->dispatch($message);

            return new JsonResponse('Created Successful', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            $this->log($e);

            return new JsonResponse($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Route("/item/order", methods={"PUT"})
     */
    public function updateItemsOrder(Request $request, MessageBusInterface $commandBus): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        try {
            $commandBus->dispatch(new UpdateItemsOrderNo($data['id'], $data['all']));

            return new JsonResponse('Updated Successful', Response::HTTP_OK);
        } catch (\Exception $e) {
            $this->log($e);

            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/item/{id}", methods={"DELETE"})
     */
    public function remove(ItemRepositoryInterface $itemRepository, int $id): JsonResponse
    {
        try {
            $result = $itemRepository->remove($id);

            return new JsonResponse(sprintf('Removed %s rows', $result), Response::HTTP_OK);
        } catch (\Exception $e) {
            $this->log($e);

            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    private function log(\Exception $e)
    {
        $this->loggerItem->warning($e->getMessage(),[
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'code' => $e->getCode()
        ]);
    }
}
