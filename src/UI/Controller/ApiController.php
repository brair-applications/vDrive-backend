<?php

namespace App\UI\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/ping")
     */
    public function index()
    {
        return new JsonResponse('Hello');
    }

    /** @Route("/test", name="test") */
    public function test()
    {
        return new JsonResponse('...');
    }
}
