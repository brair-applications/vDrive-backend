<?php

declare(strict_types=1);

namespace App\Domain\Item\Query;

interface GetItemsFromFolderQueryInterface
{
    public function getResult(int $parentId): array;
}
