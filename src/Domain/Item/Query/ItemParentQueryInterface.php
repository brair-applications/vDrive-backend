<?php

declare(strict_types=1);

namespace App\Domain\Item\Query;

interface ItemParentQueryInterface
{
    public function getData(int $parentId);
}