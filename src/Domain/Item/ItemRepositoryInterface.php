<?php

declare(strict_types=1);

namespace App\Domain\Item;

interface ItemRepositoryInterface
{
    public function find(int $id);
    public function create(array $data);
    public function remove(int $id): int;
    public function updateOrderNo(int $id, int $orderNo): void;//TODO maybe use array of id here and begin transaction,commit transaction? this is faster
}
