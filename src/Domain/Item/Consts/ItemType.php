<?php

declare(strict_types=1);

namespace App\Domain\Item\Consts;

class ItemType
{
    const TYPE_FOLDER = 'FOLDER';
    const TYPE_ITEM = 'ITEM';
    const TYPE_NOTE = 'NOTE';
}