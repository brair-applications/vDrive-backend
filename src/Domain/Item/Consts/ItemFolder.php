<?php

declare(strict_types=1);

namespace App\Domain\Item\Consts;

class ItemFolder
{
    /** first order number in folder */
    const FIRST_ORDER_ID = 0;
    const ROOT_ID = 0;
}
