<?php

declare(strict_types=1);

namespace App\Domain\Icon\Type;

use App\Domain\Item\Consts\ItemType;

class IconType
{
    const ICON_DEFAULT = 'mdi-file-outline';
    const ICON_DEFAULT_FOLDER = 'mdi-folder';
    const ICON_DEFAULT_FILE = 'mdi-file';

    public static function getIconTypes(): array
    {
        return [
            self::ICON_DEFAULT,
            self::ICON_DEFAULT_FILE,
            self::ICON_DEFAULT_FOLDER
        ];
    }

    public static function getInsertStringForMigration(): string
    {
        $insertValuesString = '';

        foreach (self::getIconTypes() as $iconName) {
            $insertValuesString .= sprintf('("%s",1),', $iconName);
        }

        return substr($insertValuesString, 0, -1);
    }
}
