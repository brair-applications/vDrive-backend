<?php

declare(strict_types=1);

namespace App\Domain\Icon\Query;

interface IconQueryInterface
{
    public function getId(string $name);
}