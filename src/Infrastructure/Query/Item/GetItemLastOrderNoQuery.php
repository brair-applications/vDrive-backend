<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Item;

use App\Domain\Item\Consts\ItemFolder;
use App\Domain\Item\Query\ItemParentQueryInterface;
use Doctrine\DBAL\Connection;

class GetItemLastOrderNoQuery extends ItemQueryAbstract implements ItemParentQueryInterface
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(int $parentId): int
    {
        $query = sprintf("SELECT max(order_id) AS 'maxOrderNo'
                  FROM %s
                  WHERE parent_id = :parentId",
            self::TABLE_NAME);

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('parentId', $parentId);
        $stmt->execute();

        $data = $stmt->fetch();

        return $this->parse($data) ?? ItemFolder::FIRST_ORDER_ID;
    }

    private function parse($data)
    {
        return (int)$data['maxOrderNo'];
    }

}
