<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Item;

abstract class ItemQueryAbstract
{
    const TABLE_NAME = 'item';
}