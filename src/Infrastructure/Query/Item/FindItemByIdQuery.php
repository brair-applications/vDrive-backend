<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Item;

use Doctrine\DBAL\Connection;

class FindItemByIdQuery extends ItemQueryAbstract
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(int $id): array
    {
        try {
            $query = sprintf("
                SELECT *
                FROM %s
                WHERE id = :id",
                self::TABLE_NAME);

            $stmt = $this->connection->prepare($query);
            $stmt->bindValue('id', $id);
            $stmt->execute();

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            throw new \Exception(sprintf("Invalid Item ID [%s]", $id));
        }
    }
}