<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Item;

use Doctrine\DBAL\Connection;

class FindItemByParentIdQuery extends ItemQueryAbstract
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(int $parentId): array
    {
        try {
            $query = sprintf("
                SELECT *
                FROM %s
                WHERE parent_id = :parent_id",
                self::TABLE_NAME);

            $stmt = $this->connection->prepare($query);
            $stmt->bindValue('parent_id', $parentId);
            $stmt->execute();

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            throw new \Exception(sprintf("Can't found parent_id [%s].", $parentId));
        }
    }
}