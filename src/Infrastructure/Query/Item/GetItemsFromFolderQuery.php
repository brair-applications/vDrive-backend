<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Item;

use App\Application\Util\ArrayUtil;
use App\Domain\Item\Query\GetItemsFromFolderQueryInterface;
use Doctrine\DBAL\Connection;

class GetItemsFromFolderQuery extends ItemQueryAbstract implements GetItemsFromFolderQueryInterface
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getResult(int $parentId): array
    {
        $query = sprintf("SELECT 
                    d.id            AS 'id', 
                    d.name          AS 'name', 
                    i.name          AS 'icon',
                    d.parent_id     AS 'parentId',
                    d.order_id      AS 'orderNo',
                    d.type          AS 'type'
                  FROM 
                    %s d 
                  JOIN 
                    icon i ON d.icon_id = i.id
                  WHERE 
                    d.parent_id = :parentId
                  ORDER BY d.order_id ASC",
            self::TABLE_NAME);

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('parentId', $parentId);
        $stmt->execute();

        $data = $stmt->fetchAll();

        return $this->parse($data, $parentId) ?? [];
    }

    private function parse(array $data, int $parentId)
    {
        ArrayUtil::aSortByOrderNo($data);

        return [
            'id' => $parentId,
            'all' =>  $data
        ];
    }
}
