<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Icon;

abstract class IconQueryAbstract
{
    const TABLE_NAME = 'icon';
}