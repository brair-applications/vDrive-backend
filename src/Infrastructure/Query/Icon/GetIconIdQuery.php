<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Icon;

use App\Domain\Icon\Query\IconQueryInterface;
use Doctrine\DBAL\Connection;

class GetIconIdQuery extends IconQueryAbstract implements IconQueryInterface
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getId(string $iconName): int
    {
            $query = sprintf("
                SELECT id
                FROM %s
                WHERE name = :name",
                self::TABLE_NAME);

            $stmt = $this->connection->prepare($query);
            $stmt->bindValue('name', $iconName);
            $stmt->execute();

            $data = $stmt->fetch();//return array or false

            $this->valid($data, $iconName);

            return $this->parse($data);
    }

    private function parse(array $data)
    {
        return (int)$data['id'];
    }

    private function valid($data, string $searchIconName)
    {
        if (!isset($data['id'])) {
            throw new \Exception(sprintf("Invalid icon name [%s]", $searchIconName));
        }
    }
}
