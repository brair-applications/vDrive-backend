<?php

declare(strict_types=1);

namespace App\Infrastructure\Messenger\Middleware;

use App\Application\Message\Command\Item\CreateItem;
use App\Domain\Item\Consts\ItemFolder;
use App\Infrastructure\Query\Item\FindItemByIdQuery;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

class ItemValidationMiddleware implements MiddlewareInterface
{
    private $findItemByIdQuery;

    public function __construct(FindItemByIdQuery $findItemByIdQuery)
    {
        $this->findItemByIdQuery = $findItemByIdQuery;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $messageCommandClass = get_class($envelope->getMessage());

            switch ($messageCommandClass) {
                case CreateItem::class:
                    /* @var $message CreateItem */
                    $message = $envelope->getMessage();
                    $parentId = $message->getParentId();
                    $itemName = $message->getName();

                    if ($parentId !== ItemFolder::ROOT_ID && !$this->findItemByIdQuery->getData($parentId)) {
                        throw new \Exception(sprintf('Could not create item [%s] in non-existent folder with ID:[%s]!', $itemName, $parentId));
                    }
//            case DeleteItem::class:
//                /* @var $msg DeleteItem */
//                $message = $envelope->getMessage();
//
//                if (!($this-findItemByIdQuery->getData($message->getParentId())) throw new \Exception('Could not delete item! Check parent_id!');
            }



        return $stack->next()->handle($envelope, $stack);
    }
}