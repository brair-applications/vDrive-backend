<?php

declare(strict_types=1);

namespace App\Infrastructure\Migrations\Image;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200702400004 extends AbstractMigration
{
    const TABLE_ITEM = 'image';

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS `'.self::TABLE_ITEM.'` (
          `id` int NOT NULL AUTO_INCREMENT,
          `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
          `size` int(10) NOT NULL,
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          `active` BIT(1),
          PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql(sprintf('CREATE INDEX idx_id ON %s (id);', self::TABLE_ITEM));
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(sprintf('DROP TABLE IF EXISTS `%s`;', self::TABLE_ITEM));
    }
}
