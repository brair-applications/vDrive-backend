<?php

declare(strict_types=1);

namespace App\Infrastructure\Migrations\Item;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200702200002 extends AbstractMigration
{
    const TABLE_ITEM = 'item';

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS `'.self::TABLE_ITEM.'` (
          `id` int NOT NULL AUTO_INCREMENT,
          `icon_id` int(10) NULL,
          `drive_id` int(10) NULL,
          `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
          `parent_id` int(10) NOT NULL,
          `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
          `order_id` int(10) NOT NULL,
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');



        $this->addSql(sprintf('CREATE INDEX idx_parent_id ON %s (parent_id);', self::TABLE_ITEM));
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(sprintf('DROP TABLE IF EXISTS `%s`;', self::TABLE_ITEM));
    }
}
