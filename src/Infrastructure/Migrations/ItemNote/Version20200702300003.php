<?php

declare(strict_types=1);

namespace App\Infrastructure\Migrations\ItemNote;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200702300003 extends AbstractMigration
{
    const TABLE_ITEM = 'item_note';

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS `'.self::TABLE_ITEM.'` (
          `id` int NOT NULL AUTO_INCREMENT,
          `item_id` int(10) NOT NULL,
          `image_id` int(10) NOT NULL,
          `category` varchar(255) COLLATE utf8mb4_unicode_ci NULL,
          `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql(sprintf('CREATE INDEX idx_item_id ON %s (item_id);', self::TABLE_ITEM));
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(sprintf('DROP TABLE IF EXISTS `%s`;', self::TABLE_ITEM));
    }
}
