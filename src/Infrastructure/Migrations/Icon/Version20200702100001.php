<?php

declare(strict_types=1);

namespace App\Infrastructure\Migrations\Icon;

use App\Domain\Icon\Type\IconType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200702100001 extends AbstractMigration
{
    const TABLE_ICON = 'icon';

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS `'.self::TABLE_ICON.'` (
          `id` int NOT NULL AUTO_INCREMENT,
          `name` varchar(30) UNIQUE,
          `active` BIT(1),
          PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql('INSERT INTO `'.self::TABLE_ICON.'` (`name`,`active`) VALUES '.IconType::getInsertStringForMigration().';');

        $this->addSql(sprintf('CREATE INDEX idx_name ON %s (name);', self::TABLE_ICON));
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(sprintf('DROP TABLE IF EXISTS `%s`;', self::TABLE_ICON));
    }
}
