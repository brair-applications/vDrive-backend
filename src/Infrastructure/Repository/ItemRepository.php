<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Item\ItemRepositoryInterface;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;

final class ItemRepository implements ItemRepositoryInterface
{
    const TABLE_NAME = 'item';

    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function updateOrderNo(int $id, int $orderNo): void
    {
        $query = sprintf("
            UPDATE %s 
            SET order_id = :order_id, updated_at = :updated_at 
            WHERE id = :id",
            static::TABLE_NAME
        );

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('id', $id);
        $stmt->bindValue('order_id', $orderNo);
        $stmt->bindValue('updated_at', ( new \DateTime())->format('Y-m-d'));

        $stmt->execute();
    }

    public function create(array $data)
    {
        $query = sprintf("
            INSERT INTO %s (
                drive_id, 
                icon_id, 
                name, 
                parent_id, 
                order_id, 
                type, 
                created_at, 
                updated_at
            ) 
            VALUES (
                :drive_id, 
                :icon_id, 
                :name, 
                :parent_id, 
                :order_id, 
                :type, 
                :created_at, 
                :updated_at
            )", static::TABLE_NAME
        );

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('drive_id', $data['drive_id']);
        $stmt->bindValue('icon_id', $data['icon_id']);
        $stmt->bindValue('name', $data['name']);
        $stmt->bindValue('parent_id', $data['parent_id']);
        $stmt->bindValue('order_id', $data['order_id']);
        $stmt->bindValue('type', $data['type']);
        $stmt->bindValue('created_at', (new \DateTime())->format('Y-m-d'));
        $stmt->bindValue('updated_at', (new \DateTime())->format('Y-m-d'));

        $stmt->execute();
    }

    public function find(int $id)
    {
        // TODO: Implement find() method.
    }

    public function remove(int $id):int
    {
        $query = sprintf("DELETE FROM %s WHERE id = :id", static::TABLE_NAME);

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('id', $id, ParameterType::INTEGER);

        $stmt->execute();

        return $stmt->rowCount();
    }
}
