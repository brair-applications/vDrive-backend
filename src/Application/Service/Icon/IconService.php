<?php

declare(strict_types=1);

namespace App\Application\Service\Icon;

use App\Domain\Icon\Type\IconType;
use App\Domain\Item\Consts\ItemType;
use App\Infrastructure\Query\Icon\GetIconIdQuery;

class IconService
{
    private $getIconIdQuery;

    public function __construct(GetIconIdQuery $getIconIdQuery)
    {
        $this->getIconIdQuery = $getIconIdQuery;
    }

    public function getDefaultIconId(string $itemType)
    {
        switch ($itemType) {
            case ItemType::TYPE_ITEM:
                return $this->getIconIdQuery->getId(IconType::ICON_DEFAULT_FILE);
            case ItemType::TYPE_FOLDER:
                return $this->getIconIdQuery->getId(IconType::ICON_DEFAULT_FOLDER);
            default:
                throw new \Exception(sprintf('Invalid item type [%s]', $itemType));
//            default:
//                return $this->iconService->getId(IconType::ICON_DEFAULT);
//                break;
        }
    }
}
