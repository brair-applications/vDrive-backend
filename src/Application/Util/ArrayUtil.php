<?php

declare(strict_types=1);

namespace App\Application\Util;

class ArrayUtil
{
    static public function aSortByOrderNo(array &$data)
    {
        usort($data, function ($a, $b) {
            return $a['orderNo'] - $b['orderNo'];
        });

        //return $data;
    }
}
