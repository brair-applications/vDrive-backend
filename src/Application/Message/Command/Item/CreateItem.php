<?php

declare(strict_types=1);

namespace App\Application\Message\Command\Item;

class CreateItem
{
    private $name;
    private $parentId;
    private $type;

    public function __construct(int $parentId, string $name, string $type)
    {
        $this->name = $name;
        $this->type = $type;
        $this->parentId = $parentId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function getType(): string
    {
        return $this->type;
    }
}