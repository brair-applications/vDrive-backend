<?php

declare(strict_types=1);

namespace App\Application\Message\Command\Item;

class UpdateItemsOrderNo
{
    private $itemId;
    private $itemsOrderNo;

    public function __construct(int $itemId, array $itemsOrderNo)
    {
        $this->itemId = $itemId;
        $this->itemsOrderNo = $itemsOrderNo;
    }

    public function getItemId(): int
    {
        return $this->itemId;
    }

    public function getItemsOrderNo(): array
    {
        return $this->itemsOrderNo;
    }

}