<?php

declare(strict_types=1);

namespace App\Application\MessageHandler\Command\Item;

use App\Application\Message\Command\Item\UpdateItemsOrderNo;
use App\Infrastructure\Repository\ItemRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateItemsOrderNoHandler implements MessageHandlerInterface
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(UpdateItemsOrderNo $command)
    {
        $itemsWithOrderNo = $command->getItemsOrderNo();

        foreach ($itemsWithOrderNo as $item) {
            $this->itemRepository->updateOrderNo((int)$item['id'], $item['orderNo']);
        }
    }
}
