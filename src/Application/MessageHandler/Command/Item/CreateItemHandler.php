<?php

declare(strict_types=1);

namespace App\Application\MessageHandler\Command\Item;

use App\Application\Message\Command\Item\CreateItem;
use App\Application\Service\Icon\IconService;
use App\Infrastructure\Query\Item\GetItemLastOrderNoQuery;
use App\Infrastructure\Repository\ItemRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateItemHandler implements MessageHandlerInterface
{
    private $getItemLastOrderNoQuery;
    private $itemRepository;
    private $iconService;

    public function __construct(
        GetItemLastOrderNoQuery $getItemLastOrderNoQuery,
        IconService $iconService,
        ItemRepository $itemRepository
    ) {
        $this->getItemLastOrderNoQuery = $getItemLastOrderNoQuery;
        $this->itemRepository = $itemRepository;
        $this->iconService = $iconService;
    }

    public function __invoke(CreateItem $command)
    {
        $lastOrderInFolder = $this->getItemLastOrderNoQuery->getData($command->getParentId());
        $icon = $this->iconService->getDefaultIconId($command->getType());

        $data = [
            'drive_id'  => 0,
            'icon_id'   => $icon,
            'name'      => $command->getName(),
            'parent_id' => $command->getParentId(),
            'order_id'  => $lastOrderInFolder + 1,
            'type'      => $command->getType(),
        ];

        $this->itemRepository->create($data);
    }
}
